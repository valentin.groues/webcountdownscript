# WebCountDownScript

Easily create sharable timer links for https://www.webcountdown.net

## Requirements

Python ≥ 2.7

## Usage

```
python webcountdown.py
```
will default to a 46 minutes timer

For a different duration use:

```
python webcountdown.py ${duration_in_minutes}
```
example:

```
python webcountdown.py 10
```

The generated link will be printed in the terminal.


