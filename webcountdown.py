import datetime
import logging
import sys
import requests

MINUTES = 46

logger = logging.getLogger(__name__)

URL = 'https://www.webcountdown.net/do.php'
DATA_TEMPLATE = {
    'do': 'c',
    'gmttzo': -1,
    'fast': 1,
    'td_month': 12,
    'td_day': 16,
    'td_year': 2021,
    'td_hour': 12,
    'td_minute': 0,
    'td_second': 0,
    'td_up': 0
}

def main():
    try:
        minutes = int(sys.argv[1])
    except (ValueError, IndexError):
        minutes = MINUTES
        print("minutes parameter not provided, using default of %s minutes" % MINUTES)
    now = datetime.datetime.now()
    target = now + datetime.timedelta(minutes=minutes)
    data = dict(DATA_TEMPLATE)
    data['td_month'] = target.month
    data['td_day'] = target.day
    data['td_year'] = target.year
    data['td_hour'] = target.hour
    data['td_second'] = target.second
    data['td_minute'] = target.minute
    response = requests.post(URL, data=data)
    print(response.url)

if __name__ == '__main__':
    main()

